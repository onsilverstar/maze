const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
let playerPosition={};

const container=document.querySelector("#container");
function mazeDisplay(map)
{
    for(let i=0; i<map.length; i++)
    {
        let row=document.createElement("div");
        row.classList.add("row");
        for(let j=0; j<map[i].length; j++)
        {
            let cell=document.createElement("div")
            if(map[i][j]==="W")
            {   container.appendChild(row)
                cell.classList.add("wall");
                row.appendChild(cell);
            }
            else if(map[i][j]===" ")
            {
                container.appendChild(row)
                cell.classList.add("block");
                row.appendChild(cell)
            }
            else if(map[i][j]==="S")
            {
                container.appendChild(row)
                cell.classList.add("start");
                row.appendChild(cell)
            }
            else
            {
                container.appendChild(row)
                cell.classList.add("final");
                row.appendChild(cell)
            }

        }
    }
}
mazeDisplay(map);
let player=document.querySelector(".player");
let playerTop=180;
let playerLeft=0;
document.addEventListener("keydown", press);

function press(evt)
{
    let tempMovements=permittedMovements(map);
    if(evt.code==="ArrowUp")
    {
        playerTop-=20;
        if(checkObjectinArray(tempMovements,playerTop, playerLeft))
        {
            player.style.top=playerTop+"px";
        }
        else
        {
            playerTop+=20;
        }
       
    }
    if(evt.code==="ArrowDown")
    {
        playerTop+=20;
        if(checkObjectinArray(tempMovements,playerTop, playerLeft))
        {
            player.style.top=playerTop+"px";
        }
        else
        {
            playerTop-=20;
        }
    }
    if(evt.code==="ArrowLeft")
    {
        playerLeft-=20;
        if(checkObjectinArray(tempMovements,playerTop, playerLeft))
            {
                player.style.left=playerLeft+"px"; 
                
            }
            else
            {
                playerLeft+=20;
            }
        
    }
    if(evt.code==="ArrowRight")
    {
        playerLeft+=20;
        if(checkObjectinArray(tempMovements,playerTop, playerLeft))
            {
                player.style.left=playerLeft+"px";
                if(playerLeft===400)
                {
                    printWin();
                }
            }
        else
            {
                playerLeft-=20;

            }
                    
    }
    playerPosition["x"]=playerLeft;
    playerPosition["y"]=playerTop;
    
}
function permittedMovements(map)
{
    let mapArray=[];
    for(let i=0; i<map.length; i++)
    {
        for(let j=0; j<map[i].length; j++)
        {
            if(map[i][j]===" "||map[i][j]==="F")
            {
                mapArray.push({y:i*20, x:j*20})
            }
            else
            {
                continue;
            }
        }
    }
    return mapArray
}
function checkObjectinArray(myArray, yValue, xValue)
{
    for(let i=0; i<myArray.length; i++)
    {
        if(myArray[i].x===xValue&&myArray[i].y===yValue)
        {
            return true;
        }
    }

    return false
}
function printWin()
{
    let winsDisplay=document.getElementById("win");
    winsDisplay.style.display="block";
}